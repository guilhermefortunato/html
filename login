<html style="background-color: rgb(41,41,41)">
    <head>
        <title>Login</title>
        <link type="text/css" href="webroot.css/style_login.css" rel="stylesheet"/>
        <script src="webroot.js/loginjs.js"></script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <div>
    <body class="geral">
        <img class="imagem1" src="webroot.images/logo.png">
        <div class="login">
            <h1> Bem-vindo ao ParqueHub </h1>
            <br>
            <form name="login">
                <label id="userName">Utilizador:</label>
                <input type="text" id="userNameID" placeholder="Nome"/><br>
                <label id="password">Palavra passe:</label>
                <input type="password" id="passwordID" placeholder="Palavra-passe"/>
            </form>
            <div class="mostrar"></div>
            <input type="checkbox" onclick="ShowPassword()"> Mostrar Palavra-passe 
            </div>
            <div class="entre">
            <button class="botao">Entre</button>
            </div>
        <div class="register">
            <p>
            <a> Não tem conta e quer estacionar? Registe-se já! </a>
            </p>
            <button onclick="abrirRegisto()" href="registo.html" class="botaoregisto"> Registar</button>
        </div>
    </body>
    </div>
</html>
